import { Component, OnInit } from '@angular/core';
import { MyserviceService } from './myservice.service';
import { MatDialog } from '@angular/material';
import { NewCmpComponent} from './new-cmp/new-cmp.component';

//  ChartJs

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'AngularApp';
  toDayDate;
  componentProperty;
  constructor(private myservice: MyserviceService, public dialog: MatDialog) { }
  openDialog() {
    const dialogRef = this.dialog.open(NewCmpComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('Dialog result: ${result}');
    });
  }
  ngOnInit() {
    this.toDayDate = this.myservice.showToday();
    console.log(this.myservice.serviceProperty);
    this.componentProperty = 'Component created';
    this.myservice.serviceProperty = this.componentProperty;
  }
}
