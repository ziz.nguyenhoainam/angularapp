import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/myservice.service';

@Component({
  selector: 'app-new-cmp',
  templateUrl: './new-cmp.component.html',
  styleUrls: ['./new-cmp.component.css']
})
export class NewCmpComponent implements OnInit {

  title = 'Month';
  text = 'Angular 9';
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  isavailable = true;
  toDay = new Date();
  jsonval = { name: 'Rox', age: '25', address: { a1: 'Mumbai', a2: 'Karnataka' } };
  toDayDate;
  newComponent = 'NewComponent Created';
  newComponentProperty;
  responseData;
  // ChartJS
  //#region ChartModel
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChart = {
    Labels: [],
    Type: '',
    Options: {},
    Legend: true,
    Data: []
  };
  public radarChart = {
    Labels: [],
    Data: [],
    Type: ''
  };
  public pieChart = {
    Labels: [],
    Data: [],
    Type: ''
  };
  public lineChart = {
    Data: [],
    Labels: [],
    Options: {},
    Colors: [],
    Legend: true,
    Type: '',
    Plugins: []
  };
  public scatterChart = {
    Data: [],
    Options: {},
    Type: ''
  };
  initBarChart() {
    this.barChart.Labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    this.barChart.Type = 'bar';
    this.barChart.Options = {
      scaleShowVerticalLines: false,
      responsive: true
    };
    this.barChart.Legend = true;
    this.barChart.Data = [
      { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
      { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
    ];
  }
  initRadarChart() {
    this.radarChart.Labels = ['Q1', 'Q2', 'Q3', 'Q4'];
    this.radarChart.Data = [
      { data: [120, 130, 180, 70], label: '2017' },
      { data: [90, 150, 200, 45], label: '2018' }
    ];
    this.radarChart.Type = 'radar';
  }
  initPieChart() {
    this.pieChart.Labels = ['Sales Q1', 'Sales Q2', 'Sales Q3', 'Sales Q4'];
    this.pieChart.Data = [120, 150, 180, 90];
    this.pieChart.Type = 'pie';
  }
  initLineChart() {
    this.lineChart.Data = [
      { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
      { data: [55, 90, 62, 91, 26, 85, 30], label: 'Series B' }
    ];
    this.lineChart.Labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    this.lineChart.Options = {
      responsive: true,
    };
    this.lineChart.Colors = [
      {
        borderColor: 'black',
        backgroundColor: 'rgba(255,0,0,0.3)',
      },
    ];
    this.lineChart.Legend = true;
    this.lineChart.Type = 'line';
    this.lineChart.Plugins = [];
  }
  initScatterChart() {
    this.scatterChart.Options = {
      responsive: true,
    };

    this.scatterChart.Data = [
      {
        data: [
          { x: 1, y: 1 },
          { x: 2, y: 3 },
          { x: 3, y: -2 },
          { x: 4, y: 4 },
          { x: 5, y: -3, r: 20 },
        ],
        label: 'Series A',
        pointRadius: 10,
      },
    ];
    this.scatterChart.Type = 'scatter';
  }

  //#endregion
  myClickFunction(event) {
    // console.log(event);
    this.isavailable = !this.isavailable;
  }
  changeMonth(event) {
    alert('Changed month from the Dropdown');
    console.log(event);
  }
  constructor(private myservice: MyserviceService) { }
  ngOnInit() {
    this.toDayDate = this.myservice.showToday();
    this.newComponentProperty = this.myservice.serviceProperty;
    this.myservice.getData().subscribe((data) => {
      this.responseData = data;
      console.log(this.responseData);
    });
    this.initBarChart();
    this.initRadarChart();
    this.initPieChart();
    this.initLineChart();
    this.initScatterChart();
  }
}
