FROM node:alpine AS build
WORKDIR /opt
RUN npm install yarn
COPY  package.json ./
RUN yarn install
COPY . ./
RUN npm run build
FROM nginx
COPY --from=build /opt/dist /usr/share/nginx/html
EXPOSE 80
